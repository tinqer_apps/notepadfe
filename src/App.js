import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import {
  MainPage,
  Footer
} from './Components';

import './App.css';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <a href="/">
            <h3
              style={{
                color: 'black',
                marginTop: '50px',
                textDecoration: 'none',
              }}>
              Modern Notepad
            </h3>
          </a>
          <small>A Free Full-featured Online Notepad</small>
          <hr />
          <Router>
            <Route exact path='/' component={MainPage} />
            {/* <Route exact path='/:noteid' component={MainPage} />
            <Route path="/share" component={MainPage} />
            <Route path="/share/:noteid" component={Sharednote} /> */}
          </Router>
          <Footer />
        </div >
      </Provider>
    );
  }
}

export default App; 