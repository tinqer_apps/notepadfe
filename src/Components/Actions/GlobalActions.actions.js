import firebase from 'firebase';
import { saveAs } from 'file-saver';

import { ADD_TAGS, CLEAR_NOTE_DATA, DISPLAY_NOTE_TO_EDITOR, CLEAR_STATUSES } from '../../redux/reducers/Notes.reducer';

/**
 * Copy the Share Link and change the UI. 
 * @param {*} document 
 */
export const copyLink = (document) => {
    let linkContainer = document.getElementById('shareLinkP');
    linkContainer.select();
    document.execCommand('copy');
    linkContainer.style.backgroundColor = 'lightgrey';
    document.getElementById('copyLinkButton').innerText = "Copied!";
};

/**
 * Create a random alphanumerical string to attach at the end of the document title
 * @param {int} customLength 
 */
export const randomString = customLength => {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let customLen = customLength ? customLength : 5;
    for (let i = 0; i < customLen; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

/**
 * Show the selected Note in the Editor.
 * @param {note} e 
 */
export const routeToSelectedNote = e => {
    window.location.replace(`/${e.DocTitle}`);
}
export const shareNote = (referThis, document, db) => {
    // Create a link that the user can share with someone else
    // and view the same notes when they show up 
    let linkExtender = randomString(10);
    let container = referThis.props.qContainer;
    let titleVal = document.getElementById("notetitleInput").value;
    if (titleVal === '') {
        document.getElementById('titleInputMessage').innerText = 'Please enter a valid title';
    } else {
        db.collection('temp_notes').doc(linkExtender).set({
            noteContent: JSON.stringify(container.getContents()),
            title: titleVal
        }, { merge: true })
            .then(() => {
                let finalLink = window.location.origin.toString() + '/share/' + linkExtender;
                referThis.setState({
                    shareLink: finalLink
                });
                referThis.showModal();
            }).catch((e) => {
                alert("There was an error creating link: " + e.message);
            });
    }
};

export const signIn = (prov, referThis, emailInput, passwordInput, errorMessageField) => {
    if (prov === "Google") {
        let provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then((result) => {
            referThis.refs.modal.hide();
            referThis.setState({
                authenticated: true,
                username: result.user.displayName,
                userid: result.user.uid
            });
        }).catch((err) => {
            referThis.setState({
                authenticated: false,
            });
        });
    }

    if (prov === "manual") {
        if (emailInput.value === '' || passwordInput.value === '') {
            errorMessageField.innerText = 'Please fill in all the fields';
        } else {
            firebase.auth().signInWithEmailAndPassword(emailInput.value, passwordInput.value).then((result) => {
                referThis.refs.manualSignInModal.hide();
                referThis.setState({
                    authenticated: true,
                    username: result.user.displayName,
                    userid: result.user.uid
                });
            }).catch((err) => {
                // document.getElementById('errorMessage').innerText = err.code;
                if (err.code === 'auth/user-not-found') {
                    firebase.auth().createUserWithEmailAndPassword(emailInput.value, passwordInput.value).then((res) => {
                        referThis.setState({
                            authenticated: true,
                            username: res.user.displayName ? res.user.displayName : res.user.email,
                            userid: res.user.uid
                        });
                        referThis.refs.manualSignInModal.hide();
                    }).catch((nError) => {
                        referThis.refs.manualSignInModal.hide();
                        referThis.setState({
                            authenticated: false,
                        });
                    });
                } else {
                    referThis.setState({
                        authenticated: false,
                    });
                }
            });
        }
    }
};

/**
 * Clear title, notedata and container. 
 * @param {*} doc 
 * @param {*} container 
 */
export const clearContainerData = (doc, container) => {
    doc.getElementById('titleInputMessage').innerText = '';
    doc.getElementById('notetitleInput').value = '';
    container.setText('');
}

export const addTag = tags => dispatch => {
    dispatch({
        type: ADD_TAGS,
        payload: {
            tags: tags,
        }
    });
}

export const pullUpNote = note => dispatch => {
    dispatch({ type: CLEAR_NOTE_DATA });
    dispatch({
        type: DISPLAY_NOTE_TO_EDITOR,
        payload: {
            ...note,
        }
    })
};

export const clearStatuses = () => dispatch => {
    dispatch({ type: CLEAR_STATUSES });
}


export const downloadNote = (data, title) => {
    // TODO: Fix how images are sent in here 
    const blob = new Blob([data], { type: "text/html;charset=utf-8" });
    saveAs(blob, `${title}.html`)
}