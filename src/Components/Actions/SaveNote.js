import React, { Component } from 'react';
import firebaseApp from '../Firebase/Firebase';
import { randomString, renderSignInModal, clearContainerData } from './GlobalActions.actions';

require('firebase/firestore');
const db = firebaseApp.firestore();

export const disableSaveButton = () => {
    document.getElementById('saveButtonDiv').style.pointerEvents = 'none';
}


class SaveNote extends Component {
    showModal = () => {
        this.refs.modal.show();
    }

    hideModal = () => {
        this.refs.modal.hide();
    }

    saveButton = () => {
        let {
            authenticated,
            currentNoteId,
            qContainer,
            userid,
            username,
        } = this.props;
        let container = qContainer;
        let d = new Date();
        let titleVal = document.getElementById("notetitleInput").value;


        if (authenticated) {
            if (titleVal === '') {
                document.getElementById('titleInputMessage').innerText = 'Please enter a valid title';
            } else {
                // If the current note is already in the DB, just update the title and contents
                if (currentNoteId) {
                    db.collection(userid)
                        .doc(currentNoteId)
                        .update({
                            notes: JSON.stringify(container.getContents()),
                            title: titleVal.trim(),
                            saveDay: d.getDay().toString(),
                            saveYear: d.getFullYear().toString(),
                            saveMonth: d.getUTCMonth().toString(),
                        }).then(() => {
                            clearContainerData(document, container);
                            window.location.replace('/');
                        })
                        .catch((err) => {
                            window.alert("There was an error saving the notes, please try again later. ", err.message);
                        });
                } else {
                    let newDocTitle = titleVal.trim() + "_" + randomString();
                    // User is signed in so store their notes
                    db.collection(userid)
                        .doc(newDocTitle.replace(/ /g, ''))
                        .set(
                            {
                                notes: JSON.stringify(container.getContents()),
                                title: titleVal.trim(),
                                saveDay: d.getDay().toString(),
                                saveYear: d.getFullYear().toString(),
                                saveMonth: d.getUTCMonth().toString(),
                                username: username,
                                DocTitle: newDocTitle.replace(/ /g, ''),
                            }, { merge: true })
                        .then((success) => {
                            // Clear up everything after note saving.
                            clearContainerData(document, container);
                        });
                }
            }
        } else {
            // User is not signed in so ask them to sign in
            this.showModal();
        }
    }

    render() {
        return (
            <div className="col-md-4" id='saveButtonDiv'
                style={{ marginRight: 'auto', marginLeft: 'auto' }}>
                <button className="btn btn-outline-primary btn-block savebutton" onClick={() => this.saveButton()}
                    style={{
                        marginBottom: '5px', marginTop: '5px', padding: '5px', fontSize: '20px'
                    }}>
                    <i className="far fa-save" /> Save
                </button>
                {
                    !this.props.authenticated ?
                        renderSignInModal(this.props.signInFunction, this.props.showManualSignInModalFunction)
                        : ''
                }
            </div>
        );
    }
}

export default SaveNote;
