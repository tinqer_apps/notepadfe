import React, { Component } from 'react';
import firebaseApp from '../Firebase/Firebase';
import { copyLink, shareNote } from './GlobalActions.actions';

const db = firebaseApp.firestore();
var Modal = require('boron/DropModal');

class ShareNote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shareLink: null,
        };
    }

    showModal = () => {
        this.refs.modal.show();
    }

    hideModal = () => {
        this.refs.modal.hide();
    }

    render() {
        return (
            <div className="col-md-4"
                style={{ marginRight: 'auto', marginLeft: 'auto' }}>
                <button className="btn btn-outline-danger btn-block" onClick={() => shareNote(this, document, db)}
                    style={{
                        marginBottom: '5px', marginTop: '5px', padding: '5px', fontSize: '20px'
                    }}>
                    <i className="fas fa-share-alt-square" /> Share
                </button>
                <Modal ref="modal" modalStyle={{ padding: '10px' }}>
                    <h4>Here's your shareable link:</h4>
                    <input type="text"
                        value={this.state.shareLink}
                        style={{ border: 'none', margin: '4px', borderBottom: '1px solid blue', background: 'transparent' }} id="shareLinkP"
                        readOnly={true} />
                    <hr />
                    <button className="btn btn-light" id="copyLinkButton"
                        onClick={() => copyLink(document)}>Copy Link!</button>
                </Modal>
            </div>
        );
    }
}

export default ShareNote;