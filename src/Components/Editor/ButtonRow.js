import React from 'react';

export default function (props) {
    const {
        authenticated,
        clearNote,
        contents,
        displayHeader,
        downloadFile,
        notesTitle,
        saveNote,
        selectedFolder,
        shareNote,
        stateFolders,
    } = props;

    return (
        <div className="row" style={{ display: displayHeader ? 'none' : '', padding: '10px' }}>
            <button
                type='button'
                className='btn btn-success btn-block col-md-4 editorButtons'
                onClick={() =>
                    saveNote(
                        contents.getContents(),
                        notesTitle || document.getElementById('notetitleInput').value,
                        selectedFolder,
                        stateFolders,
                    )}
            >
                Save Note
            </button>
            <button
                className='btn btn-primary  col-md-4 editorButtons'
                disabled
                onClick={() => shareNote()}
                style={{ display: 'none' }}
                type='button'
            >
                Share Note
                <br />
                <span className="badge badge-danger">Coming soon!</span>
            </button>
            <button
                type='button'
                className='btn btn-dark  col-md-4 editorButtons'
                onClick={() => clearNote()}
                style={{ display: authenticated ? '' : 'none' }}
            >
                New Note
            </button>
            <button
                type='button'
                className='btn btn-outline-dark editorButtons'
                onClick={() => downloadFile()}
            >
                <i className="fas fa-download"></i> Download HTML
            </button>
        </div>
    );
}