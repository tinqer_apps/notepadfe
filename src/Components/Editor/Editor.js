import React, { Component } from 'react';
import Quill from 'quill';
import { ButtonRow, Tags } from '../';
import { downloadNote } from '../Actions/GlobalActions.actions';

require('firebase/firestore');
require('./editor.css');

const toolbarOptions = [
    ['bold', 'italic'],        // toggled buttons
    ['blockquote', 'code-block'],
    ['link', 'image'],
    [{ 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent

    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'font': [] }],
    [{ 'align': [] }],
];


export const NotesTitle = (notesTitle, displayHeader) => {
    return (
        <section style={{ display: displayHeader ? 'none' : '' }}>
            {
                notesTitle ?
                    <h2>{notesTitle}</h2>
                    :
                    <input
                        className="input editorInfoInputs"
                        id="notetitleInput"
                        placeholder="Note title..."
                        style={{ width: '100%' }}
                        type="text"
                    />
            }
        </section>
    )
}

class Editor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            container: null,
            pastNoteId: null,
            selectedFolder: 'Base Folder',
            newfolderNeeded: false,
            folders: [],
        };
    }

    imageHandler = () => {
        const input = document.createElement('input');

        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.click();

        input.onchange = async () => {
            const file = input.files[0];
            const formData = new FormData();

            formData.append('image', file);

            // Save current cursor state
            const range = this.state.container.getSelection(true);

            // Move cursor to right side of image (easier to continue typing)
            this.state.container.setSelection(range.index + 1);

            // TODO: Create a method to update Firebase with the image 
            // const res = await apiPostNewsImage(formData); // API post, returns image location as string e.g. 'http://www.example.com/images/foo.png'
            const res = await this.props.uploadImage({
                title: file.name,
                file: file,
                userid: this.props.userid
            });

            // Remove placeholder image
            this.state.container.deleteText(range.index, 1);

            // Insert uploaded image
            this.state.container.insertEmbed(range.index, 'image', res);
        }
    }

    componentDidMount() {
        const container = document.getElementById('editor');
        const containerModules = {
            modules: {
                toolbar: {
                    container: toolbarOptions,
                    handlers: { image: this.imageHandler }
                }
            },
            theme: 'snow',
            placeholder: 'Compose an epic...',
        };
        const quill = new Quill(container, containerModules);

        this.setState({
            container: quill
        });
    }

    componentDidUpdate() {
        let { noteId, content, folder, folders } = this.props;
        if (noteId && content) {
            this.state.container.setContents(JSON.parse(content));
        }
        if (noteId === null && content === null) {
            this.state.container.setContents({ insert: '\n' });
        }

        if (folders && this.state.folders !== folders && this.state.folders.length < folders.length) {
            this.setState({
                folders: folders,
            });
        }

        if (folder && this.state.selectedFolder !== folder) {
            this.setState({ selectedFolder: folder })
        }
    }

    /** Handle change for the Folder dropdown */
    handleDropdownChange = e => {
        if (e.target.value === 'Create New Folder') {
            this.setState({ newfolderNeeded: true, selectedFolder: e.target.value });
        }
        else {
            this.setState({ selectedFolder: e.target.value });
        }
    }

    handleInputChange = e => {
        const newFolderName = document.getElementById('newFolderName').value;
        if (newFolderName !== '' && newFolderName !== 'Create New Folder') {
            const tempArr = this.state.folders;
            // TODO: Once you figure out how to display nested folders, do this:
            // const newFolderArray = newFolderName.split('/').filter((each) => { return each != '' });
            // let result = newFolderArray.concat(tempArr);
            let result = tempArr.concat(newFolderName.trim());
            this.setState({
                newfolderNeeded: false,
                folders: result,
                selectedFolder: newFolderName.trim()
            });
        }
    }

    downloadFile = () => {
        if (document.getElementById('notetitleInput').value || this.props.notesTitle) {
            const title = document.getElementById('notetitleInput').value || this.props.notesTitle;
            downloadNote(this.state.container.root.innerHTML, title);
        }

    }

    render() {
        const {
            authenticated,
            displayHeader,
            notesTitle,
        } = this.props;
        const {
            container,
            folders,
            newfolderNeeded,
            selectedFolder,
        } = this.state;

        return (
            <div className='container'>
                <div className="card editorSection">
                    {NotesTitle(notesTitle, displayHeader)}
                    <section id='note-info-section'>
                        <Tags addTag={this.props.addTag} tags={this.props.tags} />
                    </section>
                    <section id='note-info-section'>
                        <select
                            className='custom-select'
                            value={selectedFolder}
                            onChange={this.handleDropdownChange}
                        >
                            {
                                folders &&
                                folders.map((eachFolder, i) => {
                                    return (
                                        <option value={eachFolder} key={`${eachFolder}-${i}`}>{eachFolder}</option>
                                    )
                                })
                            }
                        </select>
                    </section>

                    {
                        newfolderNeeded &&
                        <section>
                            <input type='text' className='form-control' id='newFolderName'
                                placeholder='New folder name...'
                                onBlur={this.handleInputChange}
                            />
                            <small>For subfolders, use "/". Such as FolderOne/FolderTwo/FolderThree </small>
                        </section>
                    }
                    <br />

                    <div className="card-block" id="editor"> </div>

                    <ButtonRow
                        authenticated={authenticated}
                        clearNote={this.props.clearNote}
                        contents={container}
                        displayHeader={displayHeader}
                        notesTitle={this.props.notesTitle}
                        saveNote={!newfolderNeeded ? this.props.saveNote : null}
                        selectedFolder={selectedFolder}
                        shareNote={this.props.shareNote}
                        stateFolders={this.state.folders}
                        downloadFile={this.downloadFile}
                    />
                </div>
            </div>
        );
    }
}

export default Editor;