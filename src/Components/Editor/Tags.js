import React, { Component } from 'react';

require('./editor.css');

class Tags extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stateTags: [],
        }
    }

    componentDidUpdate() {
        if (this.state.stateTags !== this.props.tags) {
            this.setState({
                stateTags: this.props.tags,
            });
        }
    }

    // TODO: See if we can use redux to update instead of local state;
    handleKeyDown = e => {
        const evKeys = ['Tab', ','];
        if (evKeys.includes(e.key)) {
            e.preventDefault();
            let tagValue = document.getElementById('tagInput').value;
            if (tagValue !== '') {
                let tags = this.state.stateTags;
                tagValue = tagValue.trim();
                tags.push(tagValue);
                this.props.addTag(tags);
                this.setState({
                    stateTags: tags,
                });
                document.getElementById('tagInput').value = '';
            }
        }
    }

    handleRemoveTag = tag => {
        const item = this.state.stateTags.indexOf(tag);
        let arr = this.state.stateTags;
        if (item === 0) {
            this.setState({
                stateTags: [],
            });
            this.props.addTag([]);
            return;
        }
        if (item > -1) {
            arr.splice(item, 1);
            this.props.addTag(arr);
            this.setState({
                stateTags: arr,
            })
        }
    }

    render() {
        return (
            <div>
                <input
                    id='tagInput'
                    className='input editorInfoInputs'
                    placeholder='Tags. Ex: #Biology, #Javascript...'
                    onKeyDown={this.handleKeyDown}
                />
                <br />
                {
                    this.state.stateTags.map((eachTag, i) => {
                        return (
                            <span className="badge badge-light eachTagClass" key={`${eachTag}-${i}`}>
                                {eachTag}
                                <button className='btn' id='deleteTagButton' onClick={() => this.handleRemoveTag(eachTag)}>
                                    <i className='fas fa-times'></i>
                                </button>
                            </span>
                        );
                    })
                }
            </div >
        );
    }
};

export default Tags;