import React, { Component } from 'react';

class ErrorPage extends Component {
    render() {
        return (
            <div className='container'>
                <div className="card">
                    <h2 className="card-header">404</h2>
                    <h4>{this.props.errormessage}</h4>
                </div>
            </div>
        );
    }
}

export default ErrorPage;