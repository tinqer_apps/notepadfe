import firebase from 'firebase';
import firebaseApp from '../Firebase/Firebase';
import { randomString } from '../Actions/GlobalActions.actions';
import { NOTE_SAVED_SUCCESS, SAVING_NOTES, NOTE_SAVE_FAILURE, CLEAR_NOTE_DATA } from '../../redux/reducers/Notes.reducer';
import { GET_RECENT_NOTES_START, GET_RECENT_NOTES_SUCCESS, GET_RECENT_NOTES_FAILURE, GETTING_FOLDERS_AND_TAGS, GETTING_FOLDERS_AND_TAGS_SUCCCESS, GETTING_FOLDERS_AND_TAGS_ERROR } from '../../redux/reducers/Organization.reducer';
import { clearLocalStorage } from '../../Utils/LocalStorage.util'

const db = firebaseApp.firestore();
const storage = firebaseApp.storage().ref();

export const signOut = () => dispatch => {
    dispatch({ type: 'SIGNING_USER_OUT' });
    firebase.auth().signOut().then(() => {
        dispatch({ type: 'USER_SIGNED_OUT', });
        dispatch({ type: CLEAR_NOTE_DATA, });
        clearLocalStorage();
        window.location.reload();
    })
}

export const loginGooglePopup = () => dispatch => {
    let provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then((result) => {
        // TODO: Only send some of the information that's needed such as: 
        /**
         * uid, displayName, photoUrl, email, emailVerified, phoneNumber, lastLoginAt, createdAt
         */
        dispatch({
            type: 'LOGIN_SUCCESSFUL',
            payload: {
                authenticated: true,
                userid: result.user.uid,
                name: result.user.displayName,
                photoUrl: result.user.photoURL,
                email: result.user.email,
                emailVerified: result.user.emailVerified,
                phoneNumber: result.user.phoneNumber,
                // userJSON: result.user.toJSON().valueOf(),
            }
        })
    }).catch((err) => {
        dispatch({
            type: 'LOGIN_ERROR',
            payload: err.message,
        })
    });
}

export const loginWithEmailAndPassword = (email, password) => dispatch => {
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then((result) => {
            dispatch({
                type: 'LOGIN_SUCCESSFUL',
                payload: {
                    authenticated: true,
                    userid: result.user.uid,
                    username: result.user.displayName,
                }
            })
        })
        .catch((err) => {
            dispatch({
                type: 'LOGIN_ERROR',
                payload: err.message,
            })
        });
}

export const registerUser = (email, password) => dispatch => {
    firebase.auth().createUserWithEmailAndPassword(email, password).then((result) => {
        dispatch({
            type: 'LOGIN_SUCCESSFUL',
            payload: {
                authenticated: true,
                userid: result.user.uid,
                userJSON: result.user.toJSON(),
                username: result.user.displayName,
            }
        })
    }).catch((nError) => {
        dispatch({
            action: 'LOGIN_ERROR',
            payload: nError.message,
        })
    });
}

export const saveTagsAndFolders = (tags, dbRef, folders, previousTags) => dispatch => {
    dispatch({ type: 'SAVING_TAGS_AND_FOLDERS' });
    let result = previousTags.concat(tags);
    dbRef.set({
        tags: result,
        folders: folders,
    }, { merge: true, })
        .then(() => {
            dispatch({ type: 'SAVING_TAGS_AND_FOLDERS_SUCCESS' });
        })
        .catch((err) => {
            dispatch({
                type: NOTE_SAVE_FAILURE,
                payload: { message: err.message },
            });
        })
};

export const getRecentNotes = userid => dispatch => {
    dispatch({ type: GET_RECENT_NOTES_START });
    let rootRef = db.collection('users').doc(userid);

    rootRef.get().then((res) => {
        if (res.exists) {
            rootRef.collection('notes')
                .orderBy('date')
                .get()
                .then((result) => {
                    let tempArr = [];
                    result.forEach((eachDoc) => {
                        if (eachDoc.exists) {
                            let data = { ...eachDoc.data() };
                            data.id = eachDoc.id;
                            tempArr.push(data);
                        }
                    });
                    dispatch({
                        type: GET_RECENT_NOTES_SUCCESS,
                        payload: { recentNotes: tempArr, limitedAt: 5 }
                    });
                }).catch((err) => {
                    dispatch({
                        type: GET_RECENT_NOTES_FAILURE,
                        payload: { error: true, message: err.message },
                    });
                })
        }
        else {
            dispatch({
                type: GET_RECENT_NOTES_FAILURE,
                payload: { error: true, message: 'User has not created any notes yet.' },
            });
            rootRef.set({ folders: ['Base Folder', 'Create New Folder'], tags: [] }, { merge: true });
        }
    });

}

export const getFoldersAndTags = userid => dispatch => {
    dispatch({ type: GETTING_FOLDERS_AND_TAGS });
    db.collection('users')
        .doc(userid)
        .get()
        .then((res) => {
            if (res.exists) {
                const folders = res.data().folders ? res.data().folders
                    : ['Base Folder', 'Create New Folder'];
                dispatch({
                    type: GETTING_FOLDERS_AND_TAGS_SUCCCESS,
                    payload: {
                        tags: res.data().tags,
                        folders: folders,
                    }
                })
            }
        })
        .catch((err) => {
            dispatch({
                type: GETTING_FOLDERS_AND_TAGS_ERROR,
                payload: {
                    error: err,
                    message: err.message,
                }
            })
        })
}

export const saveNote = data => dispatch => {
    const {
        content,
        folder,
        folders,
        noteId,
        previousTags,
        tags,
        title,
    } = data;
    const rString = noteId || randomString(10);
    if (title === '') {
        dispatch({
            type: NOTE_SAVE_FAILURE,
            payload: {
                noteId: rString,
                message: 'Please enter the information required!'
            }
        });
        return;
    }

    dispatch({ type: SAVING_NOTES, payload: { content: JSON.stringify(content), message: 'Saving note...' } });
    let notesRef = db.collection('users').doc(data.userid).collection('notes').doc(rString);
    let baseRef = db.collection('users').doc(data.userid);
    notesRef.set({
        notes: JSON.stringify(content),
        title: title,
        date: new Date(),
        tags: tags,
        folder: folder,
    }, { merge: true })
        .then((result) => {
            dispatch(saveTagsAndFolders(tags, baseRef, folders, previousTags));
            dispatch({
                type: NOTE_SAVED_SUCCESS,
                payload: {
                    noteTitle: title,
                    noteId: rString,
                    content: JSON.stringify(content),
                    message: 'Great! Your note has been successfully saved!'
                },
            });
            dispatch(getRecentNotes(data.userid));
            dispatch(getFoldersAndTags(data.userid));
        })
        .catch((err) => {
            dispatch({
                type: NOTE_SAVE_FAILURE,
                payload: { noteId: rString, message: err.message },
            });
        });
}

export const uploadImage = data => dispatch => {
    const child = storage.child(`users/${data.userid}/${data.title}`);
    return child.put(data.file).then((snapshot) => {
        return child.getDownloadURL();
    }).catch((err) => {
        console.log('There was an error uploading image: ', err);
    });
}

export const clearNote = () => dispatch => {
    dispatch({ type: CLEAR_NOTE_DATA });
}