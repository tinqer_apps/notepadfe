import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer style={{ margin: '20px', bottom: '0', height: '2.5rem' }}>
                Created with
                <i className="far fa-heart" style={{ color: 'red', margin: '10px' }}></i>
                by <a href='https://www.shagunmistry.com'>Shagun Mistry</a>
            </footer>
        );
    }
}

export default Footer;