import React from 'react';

require('./loginButtonGroup.css');

const LoginButtonGroup = props => {
    if (props.authenticated) {
        return (
            <div>
                <h2>Welcome, {props.name}!</h2>
                <button type='button' className='btn btn-outline-danger' onClick={() => props.signOut()}>Log out</button>
            </div>
        );
    }

    const ManualSignInForm = () => (
        <div className='collapse' id='manualSignInCollapse'>
            <form onSubmit={() => props.loginWithEmailAndPassword(
                document.getElementById('emailInput').value,
                document.getElementById('passwordInput').value
            )}>
                <div className="form-group">
                </div>
                <input type="email" className="form-control" id="emailInput" autoComplete='current-email' placeholder="Email" required />
                <br />
                <input type="password" className="form-control" id="passwordInput" autoComplete='current-password' placeholder="Password" required />
                <br />
                <button className="btn btn-dark" id="signInManualButton" type='submit'>
                    Sign In
                </button>
            </form>
            <small id="errorMessage">{props.errorMessage}</small>
        </div>
    )

    return (
        <div className='btn-group-vertical loginButtonGroup' role='group'>
            <button type="button" className="btn btn-outline-primary" onClick={() => props.loginWithGoogle()} id='signInButton'>
                <i
                    className="fab fa-google"
                    style={{ marginRight: '10px' }}>
                </i>
                Login With Google
                </button>
            <a type="button" className="btn btn-outline-dark" data-toggle="collapse" href='#manualSignInCollapse' id='signInButton'>
                <i className="fas fa-at" style={{ marginRight: '10px' }}></i>
                Login with email
            </a>
            <button type="button" className="btn btn-outline-info" onClick={() => console.log('Future not yet implemented!')} id='signInButton' disabled>
                <i className="fas fa-phone" style={{ marginRight: '10px' }}></i>
                Login with phone number
                <br />
                <span className="badge badge-danger">Coming soon!</span>
            </button>
            <ManualSignInForm />
        </div >
    )
}

export default LoginButtonGroup;