import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

import Editor from '../Editor/Editor';
import * as globalActions from '../Actions/GlobalActions.actions';
import * as firebaseActions from '../Firebase/Firebase.actions';
import LoginButtonGroup from '../LoginButtonGroup/LoginButtonGroup';
import UserOrganization from '../UserOrganization/UserOrganization';

require('./MainPage.css');
require('react-toastify/dist/ReactToastify.css')

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authenticated: false,
            data: null,
            message: null,
            userid: null,
            username: null,
        };
    }

    componentDidUpdate(prevProps) {
        const {
            notesError,
            notesLoading,
            notesMessage,
            notesSuccess,
        } = this.props;

        if (notesLoading && notesMessage && !notesSuccess) {
            toast.info(notesMessage, {
                autoClose: 1200,
                position: toast.POSITION.TOP_RIGHT,
            })
        }

        if (notesError && notesMessage) {
            toast.error(notesMessage, {
                autoClose: false,
                position: toast.POSITION.TOP_RIGHT,
            })
        }

        if (!notesLoading && notesSuccess) {
            toast.success(notesMessage, {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: false,
            })
        }

        this.props.GlobalActionsBound.clearStatuses();
    }

    signInModal = () => {
        this.refs.modal.show();
    }

    showManualSignInModal = () => {
        this.refs.modal.hide();
        this.refs.manualSignInModal.show();
    }

    signInManually = () => {
        let emailInput = document.getElementById('emailInput');
        let passwordInput = document.getElementById('passwordInput');
        this.props.firebaseActionsBound.loginWithEmailAndPassword(emailInput, passwordInput);
    }

    saveNote = (content, title, folder, allFolders) => {

        if (!this.props.authenticated) {
            toast.error('Please sign in to save notes!', {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: false
            });
        } else {
            this.props.firebaseActionsBound.saveNote({
                content: content,
                title: title,
                tags: this.props.notesTags,
                folder: folder,
                folders: allFolders,
                userid: this.props.userid,
                noteId: null || this.props.noteId,
                previousTags: this.props.tags,
            });
        }
    }

    shareNote = () => {
        if (!this.props.authenticated) {
            toast.error('Please sign in to share notes!', {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: false
            });
        }
        else {
            this.props.firebaseActionsBound.shareNote();
        }
    }

    render() {
        let {
            firebaseActionsBound,
            authenticated,
            message,
            folders,
            tags,
            name,
            userid,
        } = this.props;

        return (
            <div>
                <LoginButtonGroup
                    authenticated={authenticated}
                    errorMessage={message}
                    loginWithEmailAndPassword={firebaseActionsBound.loginWithEmailAndPassword}
                    loginWithGoogle={firebaseActionsBound.loginGooglePopup}
                    name={name}
                    signOut={firebaseActionsBound.signOut}
                />
                {
                    authenticated ?
                        <UserOrganization
                            folders={folders}
                            getFoldersAndTags={firebaseActionsBound.getFoldersAndTags}
                            getMostRecentNotes={firebaseActionsBound.getRecentNotes}
                            organizationLoading={this.props.organizationLoading}
                            pullUpNote={this.props.GlobalActionsBound.pullUpNote}
                            recentNotes={this.props.recentNotes}
                            tags={tags}
                            userid={userid}
                        />
                        : null
                }
                <Editor
                    addTag={this.props.GlobalActionsBound.addTag}
                    authenticated={authenticated}
                    clearNote={firebaseActionsBound.clearNote}
                    content={this.props.notesContent}
                    currentNoteId={this.props.match.params.noteid}
                    folder={this.props.notesFolder}
                    folders={folders}
                    noteId={this.props.noteId}
                    notesTitle={this.props.notesTitle}
                    saveNote={this.saveNote}
                    shareNote={this.shareNote}
                    tags={this.props.notesTags}
                    uploadImage={firebaseActionsBound.uploadImage}
                    userid={userid}
                // toast={toast}
                />
                <ToastContainer />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        authenticated: state.auth.authenticated,
        email: state.auth.email,
        emailVerified: state.auth.emailVerified,
        loading: state.auth.loading,
        message: state.auth.message,
        name: state.auth.name,
        noteId: state.notes.noteId,
        notesContent: state.notes.content,
        notesError: state.notes.error,
        notesFolder: state.notes.folder,
        notesLoading: state.notes.loading,
        notesMessage: state.notes.message,
        notesSuccess: state.notes.success,
        notesTags: state.notes.tags,
        notesTitle: state.notes.noteTitle,
        phoneNumber: state.auth.phoneNumber,
        photoUrl: state.auth.photoUrl,

        /** Organization -- Recent Notes Component */
        folders: state.org.folders,
        organizationLoading: state.org.organizationLoading,
        recentNotes: state.org.recentNotes,
        tags: state.org.tags,

        userid: state.auth.userid,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        firebaseActionsBound: bindActionCreators(firebaseActions, dispatch),
        GlobalActionsBound: bindActionCreators(globalActions, dispatch),
    };
}

// export default MainPage;
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainPage));