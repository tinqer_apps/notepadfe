import React, { Component } from 'react';
import SavedNotes from './SavedNotes';
import firebaseApp from '../Firebase/Firebase';

require('firebase/firestore');
require('./savedNotes.css');

const db = firebaseApp.firestore();
let showNotesClick = false;

class NotesAccordion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notes: [],
        };
    }

    loadNotes = () => {
        if (this.props.authenticated) {
            if (showNotesClick) {
                showNotesClick = false;
            } else {
                showNotesClick = true;
                const {
                    userid
                } = this.props;
                let referThis = this;

                db.collection(userid).onSnapshot(function (doc) {
                    let notes = [];
                    doc.forEach((eachNote) => {
                        notes.push(eachNote.data());
                    });
                    referThis.setState({
                        notes: notes,
                    });
                });
            }
        } else {
            this.setState({
                notes: null,
            })
        }
    }

    render() {

        var noteObj = this.state.notes;
        return (
            <div>
                <div className="accordion notesaccordion" id="accordionExample">
                    <div className="card accordionCard">
                        <div className="card-header" style={{ background: 'transparent' }} id="headingOne">
                            <h5 className="mb-0">
                                <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne" onClick={() => this.loadNotes()}>
                                    My Saved Notes
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" className="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">

                            <section className="row card-body">
                                {
                                    noteObj
                                        ? noteObj.map((data, i) => <SavedNotes {...data} key={++i} />)
                                        : <span></span>
                                }
                            </section>
                        </div>
                    </div>
                    {
                        /* 
                    <div className="card accordionCard">
                        <div className="card-header" style={{ background: 'transparent' }} id="headingTwo">
                            <h5 className="mb-0">
                                <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Shared Notes
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div className="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> 
                    */
                    }
                </div>
            </div>
        );
    }
}

export default NotesAccordion;