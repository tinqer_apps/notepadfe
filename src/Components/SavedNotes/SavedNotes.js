import React, { Component } from 'react';
import { deleteNote, routeToSelectedNote } from '../Actions/GlobalActions.actions';

class SavedNotes extends Component {
    render() {
        return (
            <div className="savedNoteCard col-md-3" id="showNoteButton" >
                <h6 className="savedNoteHeader" onClick={() => routeToSelectedNote(this.props)}>{this.props.title}</h6>
                {this.props.saveMonth} / {this.props.saveYear}
                <br />
                <button className="btn btn-outline-dark btn-block" id="deleteNoteBtn" onClick={() => deleteNote(this.props)}>Delete</button>
            </div>
        );
    }
}

export default SavedNotes;