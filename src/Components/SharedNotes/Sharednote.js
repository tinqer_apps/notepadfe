import React, { Component } from 'react';
import firebaseApp from '../Firebase/Firebase';
import Editor from '../Editor/Editor';
import ErrorPage from '../Errors/ErrorPage';

require('firebase/firestore');

const db = firebaseApp.firestore();

class Sharednote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            readOnly: true,
            exists: false,
            data: {},
        }
    };

    componentDidMount() {
        let noteid = this.props.match.params.noteid;
        let referThis = this;
        db.collection('temp_notes').doc(noteid).onSnapshot((noteinfo) => {
            if (noteinfo && noteinfo.exists) {
                let showNoteInfo = {
                    notes: noteinfo.data().noteContent,
                    title: noteinfo.data().title,
                };
                referThis.setState({
                    exists: true,
                    data: showNoteInfo,
                });
            }
        }, (e) => {
            alert("There was an error getting info needed, please try again later -- " + e.message);
            location.href.replace('/');
        });
    }

    render() {
        return (
            <div>
                {
                    this.state.exists
                        ? 
                        <Editor readOnly={true} data={this.state.data}/>
                        : <ErrorPage errormessage="The Note that you are looking for could not be found!" />
                }

            </div>
        );
    }
}

export default Sharednote;