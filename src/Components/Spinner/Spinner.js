import React from 'react';
require('./spinner.css');

export const Spinner = () => {
    return (
        <div className='card loadingCard'>
            <div className='lds-hourglass'></div>
        </div>
    )
}
