import React, { Component } from 'react';
import { Spinner } from '../Spinner/Spinner'

require('./userOrganization.css');
export default class UserOrganization extends Component {

    componentDidMount() {
        this.props.getMostRecentNotes(this.props.userid);
        this.props.getFoldersAndTags(this.props.userid);
    }

    render() {

        const {
            folders,
            tags,
        } = this.props;

        if (this.props.organizationLoading) {
            return <Spinner />
        }

        return (
            <div className='container'>
                <div className='accordion organizationAccordion'>
                    <div className='card accordionCard'>
                        <h5 className='card-header accordionCardHeader'>
                            <a data-toggle="collapse" href='#folderCollapse' style={{ textDecoration: 'none' }}>
                                <i className="fas fa-caret-right"></i>  Folders
                            </a>
                        </h5>
                        <div className='collapse' id='folderCollapse'>
                            <div className='card-body'>
                                {
                                    folders &&
                                    folders.filter((e) => e !== 'Create New Folder').map((eachFolder, i) => {
                                        return (
                                            <span className="badge badge-success each-folder-badge" key={`${eachFolder}-${i}`}>
                                                {eachFolder}
                                            </span>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className='card accordionCard'>
                    <h5 className='card-header accordionCardHeader'>
                        <a data-toggle="collapse" href='#tagsCollapse' style={{ textDecoration: 'none' }}>
                            <i className="fas fa-caret-right"></i> Tags
                        </a>
                    </h5>
                    <div className='collapse' id='tagsCollapse'>
                        <div className='card-body'>
                            {
                                tags.map((eachTag, i) => {
                                    return (
                                        <span className="badge badge-info each-folder-badge" key={`${eachTag}-${i}`}>
                                            {eachTag}
                                        </span>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
                <div className='card accordionCard'>
                    <h5 className='card-header accordionCardHeader'>
                        <a data-toggle="collapse" href='#mostRecentNotesCollapse' style={{ textDecoration: 'none' }}>
                            <i className="fas fa-caret-right"></i> Most recent notes
                        </a>
                    </h5>
                    <div className='collapse' id='mostRecentNotesCollapse'>
                        <div className='card-body'>
                            {
                                this.props.recentNotes && this.props.recentNotes.length > 0
                                    ? this.props.recentNotes.map((eachNote, i) => {
                                        return (
                                            <span
                                                className="badge badge-primary each-folder-badge note-badge"
                                                key={`${eachNote.id}-${i}`}
                                                onClick={() => this.props.pullUpNote(eachNote)}
                                            >
                                                {eachNote.title}
                                            </span>
                                        )
                                    })
                                    : <p>Get started by saving a note below!</p>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}