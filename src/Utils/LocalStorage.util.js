export const saveSerializedValue = (key, value) => {
    try {
        const serializedState = JSON.stringify(value);
        localStorage.setItem(key, serializedState);
    } catch (e) {
        // Ignore errors
    }
}

export const clearLocalStorage = () => {
    window.localStorage.clear();
}