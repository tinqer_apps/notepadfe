const initialState = {
    authenticated: false,
    email: '',
    emailVerified: '',
    loading: false,
    message: '',
    name: '',
    phoneNumber: '',
    photoUrl: '',
    userid: '',
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN_SUCCESSFUL':
            return {
                ...state,
                authenticated: action.payload.authenticated,
                email: action.payload.email,
                emailVerified: action.payload.emailVerified,
                name: action.payload.name,
                phoneNumber: action.payload.phoneNumber,
                photoUrl: action.payload.photoUrl,
                userid: action.payload.userid,
                message: '',
            };
        case 'LOGIN_ERROR':
            return {
                ...state,
                message: action.payload,
            };
        case 'USER_SIGNED_OUT':
            return {
                ...initialState
            };
        default:
            return state;
    }
};