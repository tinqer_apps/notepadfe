const initialState = {
    error: false,
    loading: false,
    content: null,
    message: '',
    noteId: null,
    success: false,
    tags: [],
    recentNotes: [],
};

export const ADD_TAGS = 'ADD_TAGS';
export const CLEAR_NOTE_DATA = 'CLEAR_NOTE_DATA';
export const CLEAR_STATUSES = 'CLEAR_STATUSES';
export const DISPLAY_NOTE_TO_EDITOR = 'DISPLAY_NOTE_TO_EDITOR';
export const NOTE_SAVE_FAILURE = 'NOTE_SAVE_FAILURE';
export const NOTE_SAVED_SUCCESS = 'NOTE_SAVED_SUCCESS';
export const SAVING_NOTES = 'SAVING_NOTES';

// TODO: Blacklist the message and error reducer.
export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_TAGS:
            return {
                ...state,
                tags: action.payload.tags,
            };
        case SAVING_NOTES:
            return {
                ...state,
                loading: true,
                content: action.payload.content,
                message: action.payload.message,
            };
        case NOTE_SAVED_SUCCESS:
            return {
                ...state,
                error: false,
                loading: false,
                message: action.payload.message,
                noteId: action.payload.noteId,
                noteTitle: action.payload.noteTitle,
                success: true,
            };
        case NOTE_SAVE_FAILURE:
            return {
                ...initialState,
                error: true,
                loading: false,
                message: action.payload.message,
                noteId: action.payload.noteId,
                success: false,
            };
        case DISPLAY_NOTE_TO_EDITOR:
            return {
                ...state,
                content: action.payload.notes,
                folder: action.payload.folder,
                noteId: action.payload.id,
                noteTitle: action.payload.title,
                tags: action.payload.tags,
            };
        case CLEAR_STATUSES:
            return {
                ...state,
                error: false,
                loading: false,
                success: false,
            };
        case CLEAR_NOTE_DATA:
            return {
                ...initialState,
                tags: [],
            };
        case 'USER_SIGNED_OUT':
            return {
                ...initialState,
            }
        default:
            return state
    }
};