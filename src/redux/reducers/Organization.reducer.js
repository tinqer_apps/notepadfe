export const GET_RECENT_NOTES_FAILURE = 'GET_RECENT_NOTES_FAILURE';
export const GET_RECENT_NOTES_START = 'GET_RECENT_NOTES_START';
export const GET_RECENT_NOTES_SUCCESS = 'GET_RECENT_NOTES_SUCCESS';
export const GETTING_FOLDERS_AND_TAGS = 'GETTING_FOLDERS_AND_TAGS';
export const GETTING_FOLDERS_AND_TAGS_ERROR = 'GETTING_FOLDERS_AND_TAGS_ERROR';
export const GETTING_FOLDERS_AND_TAGS_SUCCCESS = 'GETTING_FOLDERS_AND_TAGS_SUCCCESS';

const initialState = {
    folders: [],
    organizationErrorMessage: '',
    organizationLoading: true,
    recentNotes: [],
    recentNotesError: false,
    recentNotesLoading: false,
    tags: [],
};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_RECENT_NOTES_FAILURE:
            return {
                ...state,
                error: true,
                message: action.payload.message
            };
        case GET_RECENT_NOTES_START:
            return {
                ...state,
                organizationLoading: true,
            };
        case GET_RECENT_NOTES_SUCCESS:
            return {
                ...state,
                error: false,
                recentNotes: action.payload.recentNotes,
                recentCutoff: action.payload.limitedAt
            };
        case GETTING_FOLDERS_AND_TAGS:
            return {
                ...state,
                organizationLoading: true,
            }
        case GETTING_FOLDERS_AND_TAGS_SUCCCESS:
            return {
                ...state,
                organizationLoading: false,
                tags: action.payload.tags,
                folders: action.payload.folders,
            };
        case GETTING_FOLDERS_AND_TAGS_ERROR:
            return {
                ...state,
                organizationLoading: false,
                organizationErrorMessage: action.payload.message,
            };
        case 'USER_SIGNED_OUT':
            return {
                ...initialState,
            }
        default:
            return state
    }
};
