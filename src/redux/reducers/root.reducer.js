import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import NotesReducer from './Notes.reducer';
import OrganizationReducer from './Organization.reducer';

export default combineReducers({
    auth: AuthReducer,
    notes: NotesReducer,
    org: OrganizationReducer
});