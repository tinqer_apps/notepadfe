import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/root.reducer';
import { persistStore, persistReducer } from 'redux-persist';
// import { saveSerializedValue } from '../Utils/LocalStorage.util';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const _ = require('lodash');

/** Load local storage state info */
// const loadState = () => {
//     try {
//         const serializedState = localStorage.getItem('state');
//         if (serializedState === null) {
//             return undefined;
//         }
//         return JSON.parse(serializedState);
//     } catch (e) {
//         console.log('loadState() --> There was an error with getting State: ', e);
//         return undefined;
//     }
// };

const persistConfig = {
    key: 'root',
    storage: storage,
    stateReconciler: autoMergeLevel2,
    blacklist: ['notes']
};

const pReducer = persistReducer(persistConfig, rootReducer);

// const peristedState = loadState();

const configureStore = (initialState = {}) => {
    return createStore(
        pReducer,
        composeEnhancers(applyMiddleware(thunk))
    );
}

// const store = configureStore();

// store.subscribe(_.throttle(() => { saveSerializedValue(store.getState()); }, 1000));
export const store = configureStore();
export const persistor = persistStore(store);